<?php

class PrimeFactor
{
    public function generate($number){
        $primes = [];
        for($i = 2; $i <= $number; $i++){
            while($number % $i == 0){
                $primes[] = $i;
                $number /= $i;
            }
        }
        return $primes;
    }
}
